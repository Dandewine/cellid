package com.android.cellid.net;

public interface RestClient {
    <T> T create(Class<T> clazz);
}