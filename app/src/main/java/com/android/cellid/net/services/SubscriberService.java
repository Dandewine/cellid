package com.android.cellid.net.services;

import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PartMap;

/**
 * @author Denis_Zinkovskiy at 7/12/16.
 */

public interface SubscriberService {
    @Multipart
    @POST("update")
    Call<Void> updateProfile(@Header("Session-Token") String token,
                             @PartMap Map<String, RequestBody> data);
}
