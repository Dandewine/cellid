package com.android.cellid.net;

import android.util.Log;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestClientRetrofit implements RestClient {

    private Retrofit retrofit;

    public RestClientRetrofit() {
        init();
    }

    private void init() {
        Log.i("RestClient","Rest client initialization");
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(getLoggerInterceptor())
                .build();

        this.retrofit = new Retrofit.Builder()
                .client(client)
                .baseUrl(RestAPI.API_ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @Override
    public <T> T create(final Class<T> clazz) {
        return retrofit.create(clazz);
    }

    private static class TokenInterceptor implements Interceptor {
        private String token;


        @Override
        public Response intercept(Chain chain) throws IOException {

            Request request = chain.request();
            request = request.newBuilder()
                    .addHeader("Content-Type","application/json")
                    .build();
            return chain.proceed(request);
        }
    }

    private Interceptor getLoggerInterceptor() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return interceptor;
    }




}
