package com.android.cellid.net;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.annotation.NonNull;

import com.android.cellid.FileUtils;
import com.android.cellid.ImageUtils;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * @author Denis_Zinkovskiy at 7/15/16.
 */

public class NetworkBodyHelper {

    private NetworkBodyHelper() {
        throw new AssertionError("Cannot create instance of this class");
    }

    public static final boolean IMAGE_TO_UPLOAD_PREPARE_ENABLED = true;
    public static final int IMAGE_TO_UPLOAD_REQUIRED_SIZE = 640;
    public static final Bitmap.CompressFormat IMAGE_TO_UPLOAD_FORMAT = Bitmap.CompressFormat.JPEG;
    public static final int IMAGE_TO_UPLOAD_QUALITY = 90;
    public static final Bitmap.Config IMAGE_TO_UPLOAD_CONFIG = Bitmap.Config.ARGB_8888;

    public static void toRequestBodyMap(Map<String, RequestBody> _dstMap, String _key, File _file, String _mimeType) {
        RequestBody body = RequestBody.create(
                MediaType.parse(_mimeType != null ? _mimeType : "application/octet-stream"), _file);
        _dstMap.put(_key + "\"; filename=\"" + _file.getName() + "\"", body);
    }

    public static File getPerformedFileToUpload(Context _context, File _file) throws IOException {
        if (IMAGE_TO_UPLOAD_PREPARE_ENABLED) {
            File file = File.createTempFile("tmp", ".jpeg", _context.getCacheDir());
            ImageUtils.performToUpload(file.getAbsolutePath(), _file.getAbsolutePath(),
                    IMAGE_TO_UPLOAD_REQUIRED_SIZE, IMAGE_TO_UPLOAD_REQUIRED_SIZE,
                    IMAGE_TO_UPLOAD_FORMAT, IMAGE_TO_UPLOAD_QUALITY, IMAGE_TO_UPLOAD_CONFIG);
            return file;
        }
        return _file;
    }

    @NonNull
    public static RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(
                MediaType.parse("multipart/form-data"), descriptionString);
    }

    @NonNull
    public static MultipartBody.Part prepareFilePart(Context context, String partName, Uri fileUri) {
        File file = FileUtils.getFile(context, fileUri);

        // create RequestBody instance from file
        RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), file);

        // MultipartBody.Part is used to send also the actual file name
        return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
    }
}
