package com.android.cellid.net.services;

import com.android.cellid.model.Response;
import com.android.cellid.model.VerifyModel;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * @author Denis_Zinkovskiy at 7/11/16.
 */

public interface SessionsService {

    @FormUrlEncoded
    @POST("login")
    Call<Response> login(@Field("phone_number") String phoneNumber);

    @FormUrlEncoded
    @POST("verify")
    Call<VerifyModel> verify(@Field("phone_number") String phoneNumber, @Field("verification_code") String code);
}
