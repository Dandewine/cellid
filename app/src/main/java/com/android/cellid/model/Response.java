package com.android.cellid.model;

import com.google.gson.annotations.SerializedName;

/**
 * @author Denis_Zinkovskiy at 7/11/16.
 */

public class Response {
    @SerializedName("errors")
    private String[] errors;

    @SerializedName("message")
    private String message;

    public String getResponseMessage() {
        return errors == null ? message : errors[0];
    }

    public String[] getErrors() {
        return errors;
    }

    public void setErrors(String[] errors) {
        this.errors = errors;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
