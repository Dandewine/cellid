package com.android.cellid.model;

import com.google.gson.annotations.SerializedName;

/**
 * @author Denis_Zinkovskiy at 7/12/16.
 */

public class VerifyModel extends Response{
    @SerializedName("session_token") String sessionToken;

    public String getSessionToken() {
        return sessionToken;
    }

    public void setSessionToken(String sessionToken) {
        this.sessionToken = sessionToken;
    }
}
