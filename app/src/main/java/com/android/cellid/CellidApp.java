package com.android.cellid;

import android.app.Application;

import com.android.cellid.net.RestClient;
import com.android.cellid.net.RestClientRetrofit;

/**
 * @author Denis_Zinkovskiy at 7/11/16.
 */

public class CellidApp extends Application {

    private static RestClient restClient;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public static RestClient getRestClient() {
        if (restClient == null)
            restClient = new RestClientRetrofit();
        return restClient;
    }
}
