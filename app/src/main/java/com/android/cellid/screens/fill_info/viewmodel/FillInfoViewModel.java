package com.android.cellid.screens.fill_info.viewmodel;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.databinding.ObservableField;
import android.text.Editable;
import android.text.TextUtils;
import android.view.View;

import com.android.cellid.screens.fill_info.view.DocumentBottomSheet;
import com.android.cellid.screens.fill_info.view.FillInfoActivity;
import com.android.cellid.screens.take_selfie.view.TakeSelfieActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * @author Denis_Zinkovskiy at 7/12/16.
 */

public class FillInfoViewModel {
    private Context context;
    public ObservableField<String> firstName = new ObservableField<>();
    public ObservableField<String> lastName = new ObservableField<>();
    public ObservableField<String> birthDate = new ObservableField<>();
    public ObservableField<String> placeOfBirth = new ObservableField<>();

    private DocumentBottomSheet documentBottomSheet;

    public FillInfoViewModel(Context context) {
        this.context = context;
        documentBottomSheet = new DocumentBottomSheet();
    }

    public View.OnClickListener showDatePicker = v -> {
        Calendar calendar = Calendar.getInstance();
        DatePickerDialog pickerDialog = new DatePickerDialog(context, (view, year, monthOfYear, dayOfMonth) -> {
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, monthOfYear);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yy", Locale.getDefault());
            birthDate.set(simpleDateFormat.format(calendar.getTime()));
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        pickerDialog.show();
    };

    public void setFirstName(Editable firstName) {
        if (!TextUtils.equals(this.firstName.get(), firstName.toString())) {
            this.firstName.set(firstName.toString());
        }
    }

    public void setLastName(Editable lastName) {
        if (!TextUtils.equals(this.lastName.get(), lastName.toString())) {
            this.lastName.set(lastName.toString());
        }
    }

    public void setPlaceOfBirth(Editable placeOfBirth) {
        if (!TextUtils.equals(this.placeOfBirth.get(), placeOfBirth.toString())) {
            this.placeOfBirth.set(placeOfBirth.toString());
        }
    }

    public View.OnClickListener showBottomSheet = view ->
            documentBottomSheet.show(((FillInfoActivity) context).getSupportFragmentManager(), this.getClass().getName());

    public View.OnClickListener proceedStep = view -> {

        Intent intent = TakeSelfieActivity.getCallingIntent(context,
                documentBottomSheet.getUriFirst(),
                firstName.get(),
                lastName.get(),
                birthDate.get(),
                placeOfBirth.get()
        );
        context.startActivity(intent);
    };

    public void destroy() {
        context = null;
    }
}
