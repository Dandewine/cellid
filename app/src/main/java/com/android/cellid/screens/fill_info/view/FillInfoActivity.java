package com.android.cellid.screens.fill_info.view;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.android.cellid.R;
import com.android.cellid.databinding.FillInfoLayoutBinding;
import com.android.cellid.screens.fill_info.viewmodel.FillInfoViewModel;

public class FillInfoActivity extends AppCompatActivity {

    private FillInfoViewModel fillInfoViewModel;
    Animation slide_in_left, slide_out_right;

    public static Intent getCallingIntent(Context context) {
        return new Intent(context, FillInfoActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FillInfoLayoutBinding binding = DataBindingUtil.setContentView(this, R.layout.fill_info_layout);

        slide_in_left = AnimationUtils.loadAnimation(this,
                android.R.anim.slide_in_left);
        slide_out_right = AnimationUtils.loadAnimation(this,
                android.R.anim.slide_out_right);

        binding.viewSwitcher.setInAnimation(slide_in_left);
        binding.viewSwitcher.setOutAnimation(slide_out_right);

        setTypefaces(binding);
        fillInfoViewModel = new FillInfoViewModel(this);
        binding.setViewModel(fillInfoViewModel);
    }

    private void setTypefaces(FillInfoLayoutBinding binding) {
        Typeface ubuntuLight = getTypeface("Ubuntu-Light.ttf");
        binding.nameFIL.setTypeface(ubuntuLight);
        binding.lastNameFIL.setTypeface(ubuntuLight);
        binding.dateOfBirthFIL.setTypeface(ubuntuLight);
        binding.placeOfBirthFIL.setTypeface(ubuntuLight);
        binding.textHintFIL.setTypeface(ubuntuLight);

        Typeface ubuntuMedium = getTypeface("Ubuntu-Medium.ttf");
        binding.proceedButtonAL.setTypeface(ubuntuMedium);
    }

    private Typeface getTypeface(String typeface) {
        return Typeface.createFromAsset(this.getAssets(), typeface);
    }

    @Override
    protected void onDestroy() {
        fillInfoViewModel.destroy();
        super.onDestroy();
    }
}
