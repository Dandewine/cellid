package com.android.cellid.screens.login.viewmodel;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.databinding.ObservableField;
import android.text.Editable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.android.cellid.CellidApp;
import com.android.cellid.R;
import com.android.cellid.model.Response;
import com.android.cellid.net.services.SessionsService;
import com.android.cellid.screens.ViewModel;
import com.android.cellid.screens.verification.view.VerificationActivity;
import com.google.gson.Gson;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;

/**
 * @author Denis_Zinkovskiy at 7/11/16.
 */

public class LoginViewModel extends ViewModel {
    private SessionsService sessionsService;
    public ObservableField<String> phoneNumber = new ObservableField<>();
    private Context context;

    public LoginViewModel(Context context) {
        super(context);
        this.context = context;
        this.sessionsService = CellidApp.getRestClient().create(SessionsService.class);
    }


    private Callback<Response> loginCallback = new Callback<Response>() {
        @Override
        public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
            if (response.code() == HttpURLConnection.HTTP_OK) {
                //start Verification activity
                Intent callingIntent = VerificationActivity.getCallingIntent(context, phoneNumber.get());
                context.startActivity(callingIntent);
                Toast.makeText(context, response.body().getResponseMessage(), Toast.LENGTH_SHORT).show();
            } else {
                String string = null;
                try {
                    string = response.errorBody().string();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Response response1 = new Gson().fromJson(string, Response.class);
                showDialog(response1.getResponseMessage());
            }
        }

        @Override
        public void onFailure(Call<Response> call, Throwable t) {
            t.printStackTrace();
            Toast.makeText(context, "Error occur", Toast.LENGTH_SHORT).show();
        }
    };

    public View.OnClickListener login = v -> {
        if (isThereInternetConnection()) {
            boolean isValidated = validate(phoneNumber.get());
            if (isValidated) {
                sessionsService.login(phoneNumber.get()).enqueue(loginCallback);
            } else {
                Toast.makeText(context, R.string.error_phone_validation, Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(context, R.string.error_no_inet, Toast.LENGTH_SHORT).show();
        }
    };

    public void setPhone(Editable s) {
        if (!TextUtils.equals(phoneNumber.get(), s.toString()))
            phoneNumber.set(s.toString());
    }

    private boolean validate(String phone) {
        return Pattern.matches("^[0-9\\+]{13}$", phone);
    }

    public void destroy() {
        context = null;
    }

    private void showDialog(String s) {
        new AlertDialog.Builder(context)
                .setMessage(s)
                .setPositiveButton(android.R.string.ok, (dialog, i) -> dialog.dismiss()).show();
    }
}
