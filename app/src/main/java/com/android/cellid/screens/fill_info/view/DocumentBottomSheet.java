package com.android.cellid.screens.fill_info.view;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.cellid.R;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;

/**
 * @author Denis_Zinkovskiy at 7/13/16.
 */

public class DocumentBottomSheet extends BottomSheetDialogFragment implements View.OnClickListener {

    private static final int REQUEST_CODE_PERMISSION_STORAGE = 66;
    private static final int REQUEST_CODE_PERMISSION_CAMERA_STORAGE = 56;
    private static final int FROM_CAMERA_REQUEST_CODE = 56982;
    private boolean isDriverLicense = false;
    private Uri uriFirst, uriSecond, uriPassport;
    // private OnPhotoUploadListener listener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.document_bottom_sheet, container, false);
        v.findViewById(R.id.car_licence_DBS).setOnClickListener(this);
        v.findViewById(R.id.passport_DBS).setOnClickListener(this);
        return v;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.car_licence_DBS) {
            isDriverLicense = true;
            tryCameraIntent();
        } else if (id == R.id.passport_DBS) {
            isDriverLicense = false;
            tryCameraIntent();
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void tryCameraIntent() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            startCameraIntent();
        } else {
            if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA) ||
                    shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE) ||
                    shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                new AlertDialog.Builder(getActivity())
                        .setTitle(android.R.string.dialog_alert_title)
                        .setMessage(R.string.message_camera_permission)
                        .setPositiveButton(R.string.request_permission, (dialog, which) -> {
                            dialog.dismiss();
                            requestPermissions(new String[]{Manifest.permission.CAMERA,
                                            Manifest.permission.READ_EXTERNAL_STORAGE,
                                            Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                    REQUEST_CODE_PERMISSION_CAMERA_STORAGE);
                        })
                        .setNegativeButton(android.R.string.cancel, (dialog, which) -> dialog.dismiss()).show();
            } else
                requestPermissions(new String[]{Manifest.permission.CAMERA,
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        REQUEST_CODE_PERMISSION_CAMERA_STORAGE);
        }
    }

    private void startCameraIntent() {
        String filename = String.valueOf(Calendar.getInstance().getTimeInMillis()) + "temp";
        File outputFile;
        try {
            outputFile = File.createTempFile(filename, ".jpg", getStorageDirFile(getActivity()));
        } catch (IOException e) {
            Toast.makeText(getActivity(), "Can't create file. Please try again.", Toast.LENGTH_LONG).show();
            return;
        }

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (isDriverLicense && uriFirst == null) {
            uriFirst = Uri.fromFile(outputFile);
            Log.d("myTag", "startCameraIntent: first photo");
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uriFirst);
        } else if (isDriverLicense) {
            uriSecond = Uri.fromFile(outputFile);
            Log.d("myTag", "startCameraIntent: second Photo");
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uriSecond);
        } else {
            uriPassport = Uri.fromFile(outputFile);
            Log.d("myTag", "startCameraIntent: passport");
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uriPassport);
        }


        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            getActivity().startActivityForResult(takePictureIntent, FROM_CAMERA_REQUEST_CODE);
        }
    }

    private File getStorageDirFile(Context context) {
        String dirPath = Environment.getExternalStorageDirectory() + File.separator + context.getString(R.string.app_name);

        File fileDir = new File(dirPath);

        if (!fileDir.exists()) {
            fileDir.mkdirs();
        }

        return fileDir;
    }

    public Uri[] getUriFirst() {
        return isDriverLicense ? new Uri[]{uriFirst, uriSecond} : new Uri[]{uriPassport};
    }
}
