package com.android.cellid.screens.take_selfie.view;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;

import com.android.cellid.R;
import com.android.cellid.databinding.ActivityTakeSelfieBinding;
import com.android.cellid.screens.take_selfie.viewmodel.TakeSelfieViewModel;

public class TakeSelfieActivity extends AppCompatActivity {

    private static final String FISRT_NAME_KEY = "first.name";
    private static final String LAST_NAME_KEY = "last.name";
    private static final String BDAY_KEY = "bday";
    private static final String BIRTH_PLACE_KEY = "birth_play";
    private static final String URI_KEY = "uri";

    private TakeSelfieViewModel takeSelfieViewModel;

    public static Intent getCallingIntent(Context context, Uri[] uri, String... data) {
        Intent intent = new Intent(context, TakeSelfieActivity.class);
        intent.putExtra(FISRT_NAME_KEY, data[0]);
        intent.putExtra(LAST_NAME_KEY, data[1]);
        intent.putExtra(BDAY_KEY, data[2]);
        intent.putExtra(BIRTH_PLACE_KEY, data[3]);
        intent.putExtra(URI_KEY, uri);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityTakeSelfieBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_take_selfie);
        takeSelfieViewModel = new TakeSelfieViewModel(this);


        String firstName = getIntent().getStringExtra(FISRT_NAME_KEY);
        String secondtName = getIntent().getStringExtra(LAST_NAME_KEY);
        String bday = getIntent().getStringExtra(BDAY_KEY);
        String birthPlace = getIntent().getStringExtra(BIRTH_PLACE_KEY);
        Parcelable[] uriPhoto = getIntent().getParcelableArrayExtra(URI_KEY);

        Uri[] uris;
        if (uriPhoto.length == 2) {
            uris = new Uri[]{(Uri) uriPhoto[0], (Uri) uriPhoto[1]};
        } else
            uris = new Uri[]{(Uri) uriPhoto[0]};

        takeSelfieViewModel.receiveData(uris, firstName, secondtName, bday, birthPlace);


        binding.setViewModel(takeSelfieViewModel);

        Typeface ubuntuLight = getTypeface("Ubuntu-Light.ttf");
        binding.textHintATS.setTypeface(ubuntuLight);

        Typeface ubuntuMedium = getTypeface("Ubuntu-Medium.ttf");
        binding.sendDateButtonATS.setTypeface(ubuntuMedium);
    }

    private Typeface getTypeface(String typeface) {
        return Typeface.createFromAsset(this.getAssets(), typeface);
    }
}
