package com.android.cellid.screens.verification.view;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.android.cellid.R;
import com.android.cellid.databinding.ActivityVerificationBinding;
import com.android.cellid.screens.verification.viewmodel.VerificationViewModel;

/**
 * @author Denis_Zinkovskiy at 7/12/16.
 */

public class VerificationActivity extends AppCompatActivity {

    private static final String PHONE_KEY = "phone.key";

    private VerificationViewModel viewModel;

    public static Intent getCallingIntent(Context context,String phoneNumber) {
        Intent intent = new Intent(context, VerificationActivity.class);
        intent.putExtra(PHONE_KEY,phoneNumber);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityVerificationBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_verification);

        //get phone number from previous screen
        String phoneNumber = getIntent().getStringExtra(PHONE_KEY);
        viewModel = new VerificationViewModel(this,phoneNumber);

        binding.setViewModel(viewModel);

        //setting fonts to UI elements
        Typeface ubuntuMedium = getTypeface("Ubuntu-Medium.ttf");
        binding.signInButtonAV.setTypeface(ubuntuMedium);
        binding.resentBtn.setTypeface(ubuntuMedium);

        Typeface ubuntuLight = getTypeface("Ubuntu-Light.ttf");
        binding.hint.setTypeface(ubuntuLight);

        Typeface ubuntuRegular = getTypeface("Ubuntu.ttf");
        binding.underImageText.setTypeface(ubuntuRegular);
    }

    private Typeface getTypeface(String typeface) {
        return Typeface.createFromAsset(this.getAssets(), typeface);
    }

    @Override
    protected void onDestroy() {
        viewModel.destroy();
        super.onDestroy();
    }
}
