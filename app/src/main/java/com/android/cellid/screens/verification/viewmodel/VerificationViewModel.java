package com.android.cellid.screens.verification.viewmodel;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.ObservableField;
import android.text.Editable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.android.cellid.CellidApp;
import com.android.cellid.Constants;
import com.android.cellid.R;
import com.android.cellid.model.Response;
import com.android.cellid.model.VerifyModel;
import com.android.cellid.net.services.SessionsService;
import com.android.cellid.screens.ViewModel;
import com.android.cellid.screens.fill_info.view.FillInfoActivity;
import com.android.cellid.screens.verification.view.VerificationActivity;
import com.google.gson.Gson;

import java.io.IOException;
import java.net.HttpURLConnection;

import retrofit2.Call;
import retrofit2.Callback;

/**
 * @author Denis_Zinkovskiy at 7/12/16.
 */

public class VerificationViewModel extends ViewModel {

    public ObservableField<String> code = new ObservableField<>();
    private SessionsService sessionsService;
    private String phoneNumber;

    private SharedPreferences editor;

    public VerificationViewModel(VerificationActivity context, String phoneNumber) {
        super(context);
        this.phoneNumber = phoneNumber;
        sessionsService = CellidApp.getRestClient().create(SessionsService.class);

        editor = context.getSharedPreferences(Constants.SHARED_PREF_NAME, Context.MODE_PRIVATE);
    }

    //region Callbacks
    private Callback<Response> resendCallback = new Callback<Response>() {
        @Override
        public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
            Toast.makeText(context, response.body().getResponseMessage(), Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onFailure(Call<Response> call, Throwable t) {
            Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
        }
    };

    private Callback<VerifyModel> verifyCallback = new Callback<VerifyModel>() {
        @Override
        public void onResponse(Call<VerifyModel> call, retrofit2.Response<VerifyModel> response) {
            if (response.code() == HttpURLConnection.HTTP_OK) {
                Toast.makeText(context, R.string.verify_successfull, Toast.LENGTH_SHORT).show();
                editor.edit()
                        .putString(Constants.SHARED_PREF_KEY, response.body().getSessionToken())
                        .apply();
                startFillProfileActivity();
            } else {
                String string = null;
                try {
                    string = response.errorBody().string();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Response response1 = new Gson().fromJson(string, Response.class);
                showDialog(response1.getResponseMessage());
            }
        }

        @Override
        public void onFailure(Call<VerifyModel> call, Throwable t) {
            Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
        }
    };
    //endregion

    //region OnClickListeners
    //resent login request with phone as param

    public View.OnClickListener resend = view -> {
        if (isThereInternetConnection()) {
            Toast.makeText(context, R.string.sending, Toast.LENGTH_SHORT).show();
            sessionsService.login(phoneNumber).enqueue(resendCallback);
        } else
            Toast.makeText(context, R.string.error_no_inet, Toast.LENGTH_SHORT).show();
    };

    //back to previous screen
    public View.OnClickListener back = v -> ((Activity) context).onBackPressed();

    //send verify request
    public View.OnClickListener verify = view -> {
        if (isThereInternetConnection()) {
            sessionsService.verify(phoneNumber, code.get()).enqueue(verifyCallback);
        } else
            Toast.makeText(context, R.string.error_no_inet, Toast.LENGTH_SHORT).show();
    };
    //endregion

    public void setCode(Editable code) {
        if (!TextUtils.equals(this.code.get(), code.toString())) {
            this.code.set(code.toString());
        }
    }

    private void startFillProfileActivity() {
        Intent intent = FillInfoActivity.getCallingIntent(context);
        context.startActivity(intent);
    }


    private void showDialog(String s) {
        new AlertDialog.Builder(context)
                .setMessage(s)
                .setPositiveButton(android.R.string.ok, (dialog, i) -> dialog.dismiss()).show();
    }
}
