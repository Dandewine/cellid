package com.android.cellid.screens;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * @author Denis_Zinkovskiy at 7/15/16.
 */

public class ViewModel {
    protected Context context;

    public ViewModel(Context context) {
        this.context = context;
    }

    protected boolean isThereInternetConnection() {
        ConnectivityManager connectivityManager = (ConnectivityManager) this.context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnectedOrConnecting());
    }

    public void destroy(){
        context = null;
    }

}
