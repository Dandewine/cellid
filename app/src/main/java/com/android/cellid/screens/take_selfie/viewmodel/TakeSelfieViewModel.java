package com.android.cellid.screens.take_selfie.viewmodel;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.cellid.CellidApp;
import com.android.cellid.Constants;
import com.android.cellid.FileUtils;
import com.android.cellid.R;
import com.android.cellid.net.NetworkBodyHelper;
import com.android.cellid.net.services.SubscriberService;
import com.android.cellid.screens.ViewModel;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.android.cellid.net.NetworkBodyHelper.createPartFromString;

/**
 * @author Denis_Zinkovskiy at 7/13/16.
 */

public class TakeSelfieViewModel extends ViewModel {
    private static final int REQUEST_CODE_PERMISSION_CAMERA_STORAGE = 23;
    private static final int FROM_CAMERA_REQUEST_CODE = 22;
    private SubscriberService subscriberService;

    private String firstName, lastName, bDay, birthPlace, session_token;
    private Uri uriSelfie;
    Uri[] uriDoc;

    public TakeSelfieViewModel(Context context) {
        super(context);
        subscriberService = CellidApp.getRestClient().create(SubscriberService.class);
        SharedPreferences preferences = context.getSharedPreferences(Constants.SHARED_PREF_NAME, Context.MODE_PRIVATE);
        session_token = preferences.getString(Constants.SHARED_PREF_KEY, null);
    }

    public View.OnClickListener takePhoto = view -> tryCameraIntent();

    public View.OnClickListener sendData = view -> {

        RequestBody firstName = createPartFromString(this.firstName == null ? "" : this.firstName);
        RequestBody lastName = createPartFromString(this.lastName == null ? "" : this.lastName);
        RequestBody bDay = createPartFromString(this.bDay == null ? "" : this.bDay);
        RequestBody birthPlace = createPartFromString(this.birthPlace == null ? "" : this.birthPlace);

        HashMap<String, RequestBody> bodyHashMap = new HashMap<>();
        bodyHashMap.put("first_name", firstName);
        bodyHashMap.put("last_name", lastName);
        bodyHashMap.put("date_of_birth", bDay);
        bodyHashMap.put("place_of_birth", birthPlace);


       /* bodyHashMap.put("first_name", createPartFromString("testName"));
        bodyHashMap.put("last_name", createPartFromString("testLastName"));
        bodyHashMap.put("date_of_birth", createPartFromString("testBDAY"));
        bodyHashMap.put("place_of_birth", createPartFromString("testPLACEBDAY"));*/

        File selfie = FileUtils.getFile(context, uriSelfie);
        File doc1, doc2 = null;
        boolean isPassport = uriDoc.length == 1;

        if (!isPassport) {
            doc2 = FileUtils.getFile(context, uriDoc[1]);
        }

        doc1 = FileUtils.getFile(context, uriDoc[0]);


        if (selfie != null && selfie.exists()) {
            File fileToUpload, doc1ToUpload, doc2ToUpload;
            try {
                fileToUpload = NetworkBodyHelper.getPerformedFileToUpload(context, selfie);
                doc1ToUpload = NetworkBodyHelper.getPerformedFileToUpload(context, doc1);
                if (!isPassport) {
                    doc2ToUpload = NetworkBodyHelper.getPerformedFileToUpload(context, doc2);
                    NetworkBodyHelper.toRequestBodyMap(bodyHashMap, "document_photos[]", doc2ToUpload, FileUtils.getMimeType(doc2ToUpload));
                }
                NetworkBodyHelper.toRequestBodyMap(bodyHashMap, "photo", fileToUpload, FileUtils.getMimeType(fileToUpload));
                NetworkBodyHelper.toRequestBodyMap(bodyHashMap, "document_photos[]", doc1ToUpload, FileUtils.getMimeType(doc1ToUpload));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (!session_token.isEmpty()) {
            subscriberService.updateProfile(session_token, bodyHashMap).enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    Log.d("myTag", "onResponse: " + response.body());
                    Toast.makeText(context, "Your profile has been successfully updated", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        } else
            Toast.makeText(context, "You should pass verification step, your token is empty", Toast.LENGTH_SHORT).show();

    };

    @TargetApi(Build.VERSION_CODES.M)
    private void tryCameraIntent() {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            startCameraIntent();
        } else {
            if (((Activity) context).shouldShowRequestPermissionRationale(Manifest.permission.CAMERA) ||
                    ((Activity) context).shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE) ||
                    ((Activity) context).shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                new AlertDialog.Builder(context)
                        .setTitle(android.R.string.dialog_alert_title)
                        .setMessage(R.string.message_camera_permission)
                        .setPositiveButton(R.string.request_permission, (dialog, which) -> {
                            dialog.dismiss();
                            ((Activity) context).requestPermissions(new String[]{Manifest.permission.CAMERA,
                                            Manifest.permission.READ_EXTERNAL_STORAGE,
                                            Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                    REQUEST_CODE_PERMISSION_CAMERA_STORAGE);
                        })
                        .setNegativeButton(android.R.string.cancel, (dialog, which) -> dialog.dismiss()).show();
            } else
                ((Activity) context).requestPermissions(new String[]{Manifest.permission.CAMERA,
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        REQUEST_CODE_PERMISSION_CAMERA_STORAGE);
        }
    }

    public void receiveData(Uri[] photo, String... data) {
        this.firstName = data[0];
        this.lastName = data[1];
        this.bDay = data[2];
        this.birthPlace = data[3];
        this.uriDoc = photo;
    }

    private void startCameraIntent() {
        String filename = String.valueOf(Calendar.getInstance().getTimeInMillis()) + "temp";
        File outputFile;
        try {
            outputFile = File.createTempFile(filename, ".jpg", getStorageDirFile(context));
        } catch (IOException e) {
            Toast.makeText(context, "Can't create file. Please try again.", Toast.LENGTH_LONG).show();
            return;
        }

        uriSelfie = Uri.fromFile(outputFile);

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uriSelfie);

        if (takePictureIntent.resolveActivity(context.getPackageManager()) != null) {
            ((Activity) context).startActivityForResult(takePictureIntent, FROM_CAMERA_REQUEST_CODE);
        }
    }

    private File getStorageDirFile(Context context) {
        String dirPath = Environment.getExternalStorageDirectory() + File.separator + context.getString(R.string.app_name);

        File fileDir = new File(dirPath);

        if (!fileDir.exists()) {
            fileDir.mkdirs();
        }
        return fileDir;
    }


}
