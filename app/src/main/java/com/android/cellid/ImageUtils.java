package com.android.cellid;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.text.TextUtils;

import java.io.FileOutputStream;
import java.io.IOException;

public class ImageUtils {

    public static boolean performToUpload(String _outFile, String _rawImageFile, int _reqWidth, int _reqHeight, Bitmap.CompressFormat compressFormat, int _quolity, Bitmap.Config bitmapConfig) throws IllegalArgumentException, IOException {
        if (TextUtils.isEmpty(_outFile)) {
            throw new IllegalArgumentException("_outFile not may be null");
        } else if (TextUtils.isEmpty(_rawImageFile)) {
            throw new IllegalArgumentException("_rawImageFile not may be null");
        } else if (_reqWidth <= 0 || _reqHeight <= 0) {
            throw new IllegalArgumentException("_reqWidth and _reqHeight has to be greater than 0 (w, h): " + _reqWidth + ", " + _reqHeight);
        }

        final Bitmap bmp = getResizedAndRotatedBitmap(_rawImageFile, _reqWidth, _reqHeight, bitmapConfig);
        return saveToFile(_outFile, bmp, compressFormat, _quolity);
    }

    public static boolean saveToFile(String _outFile, Bitmap bmp, Bitmap.CompressFormat _compressFormat, int _quolity) {
        boolean resultOk = false;
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(_outFile);
            bmp.compress(_compressFormat, _quolity, out);
            resultOk = true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return resultOk;
    }

    public static Bitmap getResizedAndRotatedBitmap(String _pathToFile, int _reqWidth, int _reqHeight, Bitmap.Config _bitmapConfig) throws IOException {
        ExifInterface exif = new ExifInterface(_pathToFile);
        int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
        int rotationInDegrees = exifToDegrees(rotation);

        Matrix matrix = new Matrix();
        if (rotation != 0f) {
            matrix.preRotate(rotationInDegrees);
        }

        final Bitmap sourceBitmap = decodeSampledBitmapFromFile(_pathToFile, _reqWidth, _reqHeight, _bitmapConfig);
        final Bitmap adjustedBitmap = Bitmap.createBitmap(sourceBitmap, 0, 0, sourceBitmap.getWidth(), sourceBitmap.getHeight(), matrix, true);
        return adjustedBitmap;
    }

    private static int exifToDegrees(int _rotation) {
        if (_rotation == ExifInterface.ORIENTATION_ROTATE_90) {
            return 90;
        } else if (_rotation == ExifInterface.ORIENTATION_ROTATE_180) {
            return 180;
        } else if (_rotation == ExifInterface.ORIENTATION_ROTATE_270) {
            return 270;
        }
        return 0;
    }

    public static Bitmap decodeSampledBitmapFromFile(String _pathToFile, int _reqWidth, int _reqHeight, Bitmap.Config _bitmapConfig) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(_pathToFile, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, _reqWidth, _reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        options.inPreferredConfig = _bitmapConfig;
        return BitmapFactory.decodeFile(_pathToFile, options);
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options _options, int _reqWidth, int _reqHeight) {
        // Raw height and width of image
        final int height = _options.outHeight;
        final int width = _options.outWidth;
        int inSampleSize = 1;

        if (height > _reqHeight || width > _reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > _reqHeight
                    || (halfWidth / inSampleSize) > _reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }
}
