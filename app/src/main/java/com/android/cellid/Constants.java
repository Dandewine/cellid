package com.android.cellid;

/**
 * @author Denis_Zinkovskiy at 7/15/16.
 */

public class Constants {
    public static final String SHARED_PREF_NAME = "session";
    public static final String SHARED_PREF_KEY = "session_token";
}
